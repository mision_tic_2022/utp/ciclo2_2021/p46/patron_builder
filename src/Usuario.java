public class Usuario {
    /************
     * Atributos
     *************/
    private String nombre;
    private String apellido;
    private String email;
    private String telefono;
    private String direccion;

    public Usuario(UsuarioBuilder objBuilder) {

        if (objBuilder.getApellido() == null) {
            throw new IllegalArgumentException("Se necesita por lo menos el apellido para construir el objeto Usuario");
        } else {
            this.nombre = objBuilder.getNombre();
            this.apellido = objBuilder.getApellido();
            this.telefono = objBuilder.getTelefono();
            this.direccion = objBuilder.getDireccion();
            this.email = objBuilder.getEmail();
        }
    }

    @Override
    public String toString() {
        return "Usuario [apellido=" + apellido + ", direccion=" + direccion + ", email=" + email + ", nombre=" + nombre
                + ", telefono=" + telefono + "]";
    }

    

}